<?php


require_once ('setup.php');
/** @var  $carbon*/
/** @var  $checkinData*/
/** @var  $dbh*/

try {
    $dbh = new PDO(
        'mysql:dbname=project;host=mysql',
        $_ENV['username'],
        $_ENV['password']
    );

    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e){
    // We should log this
    die('Unable to establish a database connection');
}

if (!empty($_POST)) {

    if (filter_var($_POST['rating'], FILTER_VALIDATE_INT, array("options" => array("min_range"=>1, "max_range"=>5))) === false) {
        echo 'invalid rating';
        die();
    }

    if (filter_var(strlen($_POST['name']), FILTER_VALIDATE_INT, array("options" => array("min_range"=>3, "max_range"=>20))) === false) {
        echo 'invalid name';
        die();
    }

    if (filter_var(strlen($_POST['review']), FILTER_VALIDATE_INT, array("options" => array("min_range"=>20, "max_range"=>200))) === false) {
        echo 'invalid review';
        die();
    }

    // Build new CheckIn object
    $checkinData->name = strip_tags($_POST['name']);
    $checkinData->rating = strip_tags($_POST['rating']);
    $checkinData->review = strip_tags($_POST['review']);
    $checkinData->timestamp = $carbon::now()->toDateTimeString();
    $date = $carbon::now()->toDateTimeString();

    // Save submission to file and update reviews section


    $stmt = $dbh->prepare(
        'INSERT INTO checkins (user_name, rating, review, submitted) VALUES (:user_name, :rating, :review, :submitted)'
    );

    $stmt->execute([
            'user_name' => $checkinData->name,
            'rating'=> $checkinData->rating,
            'review'=> $checkinData->review,
            'submitted'=> $date,
        ]
    );



    $stmt = $dbh->prepare(
        'SELECT user_name, rating, review, submitted FROM checkins'
    );

    $stmt->execute();
    $checkins = $stmt->fetchAll(PDO::FETCH_ASSOC);



    foreach ($checkins as $checkin){
        $username =$checkin['user_name'];
        $review =$checkin['review'];
        $date =$checkin['submitted'];
        $rating = $checkin['rating'];
        echo "<h3>$username $rating</h3>";
        echo "<p>$review</p>";
        echo "<p><small>$date</small></p>";
    }
    die();

}

// Functions
// Convert rating number to stars
function star($rate) {
    $starRating = [];
    $increment = 0;
    $max = 5;

    while($increment < $rate) {
        array_push($starRating, "<i class='fas fa-star fa text-warning pt-1'></i>");
        $increment++;
    }

    while($max > $rate) {
        array_push($starRating, "<i class='fas fa-star fa text-secondary pt-1'></i>");
        $max--;
    }

    foreach ($starRating as $rating) {
        return $starRating;
    }
}

// Calculate and update Overall Rating
function updateOverallRating($average){
   return $average > 0 ? star($average) : '<h2>No Reviews to Display</h2>';
}


// Get data from check-ins file and prepare HTML for JavaScript

$stmt = $dbh->prepare('SELECT user_name, rating, review, submitted FROM checkins'
);

$stmt->execute();
$checkins = $stmt->fetchAll(PDO::FETCH_ASSOC);


$total = 0;
$reviewsBodyA = [];
$reviewsBodyB = [];
$starsAdded = [];

foreach ($checkins as $checkin) {
    $username =$checkin['user_name'];
    $review =$checkin['review'];
    $date =$checkin['submitted'];
    $total += $checkin['rating'];
    array_push($starsAdded, $checkin['rating']);
    array_push($reviewsBodyA, "<h3>$username</h3>");
    array_push($reviewsBodyB, "<p>$review</p><p><small>$date</small></p>");
    }
$average = round($total / count($checkins));


?>

<h2 class="p-3">Additional Information</h2>
<div class="border p-3 m-3">
    <table class="table">
        <tbody>
        <tr>
            <th scope="row">Average Rating</th>
            <td>
                    <span id="overallRating">
                    <?php
                         $thisArray = updateOverallRating($average);
                         foreach ($thisArray as $theStarts) {
                             echo $theStarts;
                         }
                        ?>
                    </span>
            </td>
        </tr>
        <tr>
            <th scope="row">Yum Yum Rating</th>
            <td>78/100</td>
        </tr>
        <tr>
            <th scope="row">Wow Factor</th>
            <td>Jaw Dropping</td>
        </tr>
        </tbody>
    </table>
</div>
<h2 class="p-3">Recent Checkins</h2>

<?php

$reviewCount = 0;
foreach ($reviewsBodyA as $thisReview){
    echo "<div class='p-3 m-3 border '>";
    echo "<div class='row m-auto ml-md-auto'>";
    echo $thisReview;
    echo '&nbsp;','&nbsp;';
    echo  "<span class='pt-1'>";
    $start = star($starsAdded[$reviewCount]);
    foreach ($start as $here){
        echo $here;
    }
    echo "</span>";
    echo "</div>";
    echo "<span class=' m-auto'>";
    echo $reviewsBodyB[$reviewCount];
    $reviewCount++;
    echo "</span>";
    echo "</div>";

}




