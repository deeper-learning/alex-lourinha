<?php

require_once ('vendor/autoload.php');
require_once('checkin.php');


$carbon = new \Carbon\Carbon();

$whoops = new \Whoops\Run();
$whoops->pushHandler(
    new \Whoops\Handler\PrettyPageHandler()
);
$whoops->register();

$dotenv = \Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$checkinData = new CheckIn();

