<?php
require_once ('setup.php');
/** @var Class $carbon*/
/** @var Class $checkinData*/
/** @var Class $dbh*/


$id = $_GET['prod'];

$stmt = $dbh->prepare(
"SELECT id, title, description, image_path FROM product WHERE id = :id "
);

$stmt->execute([
    'id'=>$id,
]);
$product_data = $stmt->fetch(PDO::FETCH_ASSOC);
$path = $product_data['image_path'];
$title = $product_data['title'];
$description = $product_data['description'];

?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS & CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style-overall.css">
    <link rel="stylesheet" href="css/style-product.css">

    <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Sonsie+One&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Fredoka+One&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Raleway&display=swap" rel="stylesheet">

    <title>Retro Dough</title>
</head>
<body class="m-auto">
<div class="container m-auto " >
    <div id="alert">
        <div class="alert alert-success" id="success-alert" hidden>
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong>Success! </strong> Your review has been submitted.
        </div>
    </div>
    <div class="row  m-3">
        <div class="col-sm-6">
            <div id="carouselExampleIndicators" class="carousel slide pt-3 pb-3" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src=<?= $path ?> class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src=<?= $path ?> class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src=<?= $path ?> class="d-block w-100" alt="...">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div class="col-sm-6 pb-3 mt-3 border border-white" style='background-color: rgba(164, 182, 254, .3); '>
            <h1 id="shopname" class="pt-3"><?= $title ?></h1>
            <p class="pr-3 pt-3"><?= $description ?>
            </p>
            <button type="button" class="btn btn-success btn-lg" data-toggle="modal"  data-target="#ModalLoginForm">Check In</button>
        </div>
    </div>



    <!--Modal-->
    <div id="ModalLoginForm" class="modal fade">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title">Check In</h1>
                </div>
                <div class="modal-body">
                    <div id="alertError">
                        <div class="alert alert-danger" id="error-alert">
                            <button type="button" class="close" data-dismiss="alert">x</button>
                            <strong>Error! </strong> Please correct errors before submission.
                        </div>
                    </div>
                    <form id="checkinForm" name="checkinForm" role="form" method="POST" action="">
                        <div class="form-group">
                            <label class="control-label">Name</label>
                            <div>
                                <input id="name" type="text" class="form-control input-lg" name="name" value="">
                                <p id="nameError"><small class="text-danger">Must be between 2 and 20 characters</small></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Rating</label>
                            <div>
                                <div class="container">
                                    <div class="row align-content-star">
                                        <div class="col-lg-12">
                                            <div class="star-rating">
                                                <span class=" fas fa-star fa-lg text-secondary" data-rating="1"></span>
                                                <span class="fas fa-star fa-lg text-secondary" data-rating="2"></span>
                                                <span class="fas fa-star fa-lg text-secondary" data-rating="3"></span>
                                                <span class="fas fa-star fa-lg text-secondary" data-rating="4"></span>
                                                <span class="fas fa-star fa-lg text-secondary" data-rating="5"></span>
                                                <input id="rating" type="hidden" name="rating" class="rating-value" value="1">

                                                <script>


                                                </script>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label mt-2">Review</label>
                                <div>
                                    <textarea rows="4" id="review" type="text" class="form-control" name="review"></textarea>
                                    <small id="charCount" class="text-danger"><span id="currentChar">0</span>/200</small>
                                    <p id="reviewErrorMin"><small class="text-danger">Minimum 20 characters</small></p>
                                    <p id="reviewErrorMax"><small class="text-danger">Max characters 200</small></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <script src="https://www.google.com/recaptcha/api.js" async defer></script>

                                <div class="g-recaptcha" data-sitekey="6LdWwVYaAAAAAKNeNXgjiArDjywnb5ncE1GGiUsn" id="recaptchaVal"> </div>
                                <div>
                                    <button id="submitButton" type="button"  class="btn btn-success">Submit</button>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="checkins"></div>
    <!-- Other Scripts -->
    <script src="https://kit.fontawesome.com/de31db8cc5.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <script src="script.js"></script>
</body>
</html>
