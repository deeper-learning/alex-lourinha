<?php

require_once ('vendor/autoload.php');
require_once('checkin.php');


$carbon = new \Carbon\Carbon();

$whoops = new \Whoops\Run();
$whoops->pushHandler(
    new \Whoops\Handler\PrettyPageHandler()
);
$whoops->register();

$dotenv = \Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

try {
    $dbh = new PDO(
        'mysql:dbname=project;host=mysql',
        $_ENV['username'],
        $_ENV['password']
    );

    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e){
    die('Unable to establish a database connection');
}
