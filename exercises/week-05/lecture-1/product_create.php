<?php

require_once 'db.php';

$newProductTitle = 'New Product';

$stmt = $dbh->prepare(
    'INSERT INTO product (title) VALUES (:title)'
    );

$stmt->execute([
    'title' => $newProductTitle
    ]
);



echo '# Rows affected: ' . $stmt->rowCount();
