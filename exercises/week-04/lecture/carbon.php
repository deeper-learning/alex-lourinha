<?php

require_once 'setup.php';

use Carbon\Carbon;

$birtday = Carbon::createFromDate(1990, 7, 24);
$age = $birtday->age;

echo 'Age: ' . $age . '<br>';
echo  'DoB: ' . $birtday->format('d/m/y');

$rightNow = Carbon::now();
$midnightToday = Carbon::today();

$difference = $rightNow->diff($midnightToday);

var_dump($difference);