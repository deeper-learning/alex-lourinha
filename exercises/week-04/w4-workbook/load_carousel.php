<?php

if (file_exists('uploads/upload_data.txt')) {
    $fileName = 'uploads/upload_data.txt';
    $file = file_get_contents($fileName);
    $newFile = unserialize($file);

    foreach ($newFile as $upload){
        $photo = $upload["photoName"];
        $name = $upload["name"];
        $date = $upload["date"];

        echo "<div class='carousel-item'>";
        echo "<img class='mt-3 d-block' width='100%' height='100%'  src='$photo'>";
        echo "<div class='carousel-caption p-5' style='position: relative; left: 0; top: 0;'>";
        echo "<p class='text-center align-text-bottom' style='font-family: \"Calligraffitti\", cursive; font-size: 1.2rem; color: black'><strong>&nbsp; &nbsp;$name &nbsp;  $date</strong></p>";
        echo "</div>";
        echo "</div>";
    }

} else {
   echo 'no file';
}