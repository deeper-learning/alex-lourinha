<?php

require_once 'setup.php';
/** @var $photoData */
/** @var  $carbon */



if (!empty($_POST)) {

    // Validation
    if (filter_var(strlen($_POST['name']), FILTER_VALIDATE_INT, array("options" => array("min_range" => 3, "max_range" => 20))) === false) {
        echo 'invalid name';
        die();
    }
    if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false) {
        echo 'invalid email';
        die();
    }

    // Hydrate new object
    $photoData->name = strip_tags($_POST['name']);
    $photoData->dob = strip_tags($_POST['dob']);
    $photoData->email = strip_tags($_POST['email']);
    $photoData->submitted = $carbon::now()->format('d/m/y');

    $photoName = "uploads/" . $carbon::now()->timestamp . ".jpg";


    move_uploaded_file($_FILES['picture']['tmp_name'], $photoName);

    $varName = $photoData->name;
    $varDate = $photoData->submitted;
    $fileName = 'uploads/upload_data.txt';
    $dataArray = [];
    array_push($dataArray, ['name' => $photoData->name, 'date' => $photoData->submitted, 'photoName' => $photoName]);
    $serialisedData = serialize($dataArray);
    file_put_contents("uploads/" . $carbon::now()->timestamp . ".txt", $serialisedData);

    if (file_exists('uploads/upload_data.txt')) {
        $file = file_get_contents($fileName);
        $newFile = unserialize($file);
        array_push($newFile, ['name' => $photoData->name, 'date' => $photoData->submitted, 'photoName' => $photoName]);
        $newArray = serialize($newFile);
        file_put_contents($fileName, $newArray);

    } else {
        file_put_contents($fileName, $serialisedData);
    }

    ?>
    <div class="carousel-item">
        <img class="mt-3 d-block" width="100%" height="100%"  src="<?=$photoName?>">
        <div class="carousel-caption p-5" style="position: relative;
    left: 0;
    top: 0;">
            <p class="text-center align-text-bottom" style="font-family: 'Calligraffitti', cursive; font-size: 1.2rem; color: black"><strong>&nbsp; &nbsp;<?=$varName?> &nbsp;  <?=$varDate?></strong></p>
        </div>
    </div>

<?php

}
?>

