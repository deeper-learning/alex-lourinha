$(document).ready(function() {
        $("#success-alert").hide();
        $("#error-alert").hide();
        $('#nameError').hide();
        $('#emailError').hide();
        $('#cropSuccess').hide();
        $('.toHide').hide();




        //  Variables
        let nameVal = false;
        let emailVal = false;
        let pictureVal = false;
        let dateVal = false;
        let data = new FormData();
        let $modal = $('#modal');
        let image = document.getElementById('my_image');
        let cropper;



        // Validation

        $('#name').on('keyup', function() {
                if (this.value.length > 20 || this.value.length < 3) {
                        $('#nameError').show();
                        nameVal = false;
                } else {
                        $('#nameError').hide();
                        nameVal = true;
                }
        });

        $('#email').on('keyup', function() {
                let isValid =  /\S+@\S+\.\S+/.test(this.value);
                if (!isValid){
                        $('#emailError').show();
                        emailVal = false;
                } else {
                        $('#emailError').hide();
                        emailVal = true;
                }
        })

        $('#myForm').on('change', function () {
                if (document.getElementById('picture').files.length > 0) {
                        pictureVal = true;
                        console.log('true')
                } else {
                        pictureVal = false;
                        console.log('false');
                }
                if (document.getElementById('dob').value == false) {
                        dateVal = false;
                } else {
                        dateVal = true;
                        console.log(Date.parse(dob));
                }
        });


        axios.get('load_carousel.php')
            .then(resp => {
                    $('#appendMe').append(resp.data);
        })



        $('#picture').change(function(event){
                let files = event.target.files;

                let done = function(url){
                        image.src = url;
                        $modal.modal('show');
                };

                if(files && files.length > 0)
                {
                        reader = new FileReader();
                        reader.onload = function(event)
                        {
                                done(reader.result);
                        };
                        reader.readAsDataURL(files[0]);
                }
        });


        $modal.on('shown.bs.modal', function() {
                cropper = new Cropper(image, {
                        aspectRatio: 1,
                        viewMode: 2,
                        ready: function () {
                                //Should set crop box data first here
                                cropper.setCropBoxData(cropBoxData).setCanvasData(canvasData);
                        }

                });
        }).on('hidden.bs.modal', function(){
                cropper.destroy();
                cropper = null;
                $('#picture').val("");

        });

        $('#crop').click(function() {
                cropper.getCroppedCanvas().toBlob((blob => {
                        data.append('picture', blob);
                }));
                $modal.modal('hide');
                $('#cropSuccess').show();
        });

        $('#submitButton').on('click', function submitForm() {
                let captchaResponse = grecaptcha.getResponse();
                logInDev(captchaResponse);

                recaptchaData = new FormData;
                recaptchaData.append('captchaResponse', captchaResponse);

                if (captchaResponse.length > 0) {
                        console.log('hello');

                        axios.post('reCaptcha.php', recaptchaData)
                            .then(resp => {
                                    logInDev(resp.data);
                                    let isCaptcha = resp.data;

                                    if (emailVal && nameVal && dateVal && pictureVal && isCaptcha) {
                                            logInDev('validation error')
                                            $('#myForm').submit();
                                            $("#error-alert").hide();
                                            $('#emailError').hide();
                                            $('#cropSuccess').hide();
                                            $('#collapseExample').hide();
                                            grecaptcha.reset();
                                            nameVal = false;
                                            emailVal = false;

                                            $('#myForm :input').val("");
                                            $("#success-alert").fadeTo(2000, 500).slideUp(500, function () {
                                                    $("#success-alert").slideUp(500);
                                            })
                                            $('#nameError').hide();

                                    }
                                    if (!nameVal) {
                                            logInDev('name error')
                                            $("#error-alert").fadeTo(2000, 500).slideUp(500, function () {
                                                    $("#error-alert").slideUp(500);
                                            })
                                            $('#nameError').show();
                                    }
                                    if (!emailVal) {
                                            logInDev('email error')
                                            $("#error-alert").fadeTo(2000, 500).slideUp(500, function () {
                                                    $("#error-alert").slideUp(500);
                                            })
                                    }
                                    if (!dateVal) {
                                            logInDev('date error')
                                            $("#error-alert").fadeTo(2000, 500).slideUp(500, function () {
                                                    $("#error-alert").slideUp(500);
                                            })
                                    }
                                    if (!pictureVal) {
                                            logInDev('picture error')
                                            $("#error-alert").fadeTo(2000, 500).slideUp(500, function () {
                                                    $("#error-alert").slideUp(500);
                                            })
                                    }
                                    if (!isCaptcha) {
                                            grecaptcha.reset();
                                    }
                            })
                            .catch(err => {
                                    logInDev(err);
                                    logInDev('error on the reCaptcha');

                            })
                } else {
                        console.log('goodbye');
                        $("#error-alert").fadeTo(2000, 500).slideUp(500, function () {
                                $("#error-alert").slideUp(500);
                        })
                }

        });


    $('#myForm').on('submit',
        function(e) {

            // Force Ajax header
            axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';


            const name = $('#name').val();
            const dob = $('#dob').val();
            const email = $('#email').val();
            e.preventDefault();
            data.append('name', name);
            data.append('dob', dob);
            data.append('email', email);

            axios.post('image_uploader.php', data)
                .then(resp => {
                    console.log(resp.data);
                    $('#appendMe').append(resp.data);

                })
                .catch(err => console.log(err));
        })


})

$('#closeModal').on('click', function (){
        $('#collapseExample').hide();
})

$('#collapseToggle').on('click', function (){
        $('#collapseExample').toggle();
})

function logInDev(data) {
        console.log(data); // you can just comment me out later on and there won't be any console logs in production!! :)
}
