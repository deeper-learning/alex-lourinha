$(document).ready(function() {
    $("#success-alert").hide();
    $("#error-alert").hide();
    $('#nameError').hide();
    $('#reviewErrorMin').hide();
    $('#reviewErrorMax').hide();

        let $star_rating = $('.star-rating .fas');

        let SetRatingStar = function() {
            return $star_rating.each(function() {
                if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
                    return $(this).removeClass('text-secondary').addClass('text-warning');
                } else {
                    return $(this).removeClass('text-warning').addClass('text-secondary');
                }
            });
        };

        $star_rating.on('click', function() {
            $star_rating.siblings('input.rating-value').val($(this).data('rating'));
            return SetRatingStar();
        });

        SetRatingStar();


    //  Variables
    let  nameVal = false;
    let  reviewVal = false;

    // Validation

    $('#name').on('keyup', function() {
        if (this.value.length > 20 || this.value.length < 3) {
            $('#nameError').show();
            nameVal = false;
        } else {
            $('#nameError').hide();
            nameVal = true;
        }
    });



    $('#review').on('keyup', function() {
        let currentChar = this.value.length;
        $('#currentChar').text(currentChar);

        if (this.value.length > 200) {
            $('#reviewErrorMax').show();
            $('#charCount').removeClass('text-success').addClass('text-danger');
            reviewVal = false;
        } else{
            $('#reviewErrorMax').hide();
            $('#charCount').removeClass('text-danger').addClass('text-success');
            reviewVal = true;
        }
        if (this.value.length < 20) {
            $('#reviewErrorMin').show();
            $('#charCount').removeClass('text-success').addClass('text-danger');
            reviewVal = false;
        } else{
            $('#reviewErrorMin').hide();
            $('#charCount').removeClass('text-danger').addClass('text-success');
            reviewVal = true;
        }
    });

    // Validation on submit

    $('#submitButton').on('click', function submitForm() {
        if (reviewVal && nameVal) {
            $('#checkinForm').submit();
            $('#ModalLoginForm').modal('hide');
            nameVal = false;
            reviewVal = false;
            $('#ModalLoginForm form :input').val("");
            $('#rating').val("1");
            $('#currentChar').text(0);
            $('#charCount').removeClass('text-success').addClass('text-danger');
            SetRatingStar();


        }  if (!nameVal) {
            $("#error-alert").fadeTo(2000, 500).slideUp(500, function () {
                $("#error-alert").slideUp(500);
            })
            $('#nameError').show();
        } if (!reviewVal) {
            $("#error-alert").fadeTo(2000, 500).slideUp(500, function () {
                $("#error-alert").slideUp(500);
            })
            if ($('#review').val().length < 20){
                $('#reviewErrorMin').show();
            }
            if ($('#review').val().length > 200){
                $('#reviewErrorMax').show();
            }
        }
    });



    axios.get('checkinscript.php')
        .then(function (response) {
            if (typeof response.data === 'string') {
                document.getElementById('checkins').innerHTML = response.data;
            } else {
                document.getElementById('checkins').innerHTML = '<h3 class="p-3 text-warning">No reviews yet</h3>';
            }
        })
        .catch(function (error) {
            console.log(error);
        })

    $('checkinForm').submit();
    // Post the form to PHP and update check-ins section with new submission
    $('#checkinForm').on('submit',
        function (e) {

            // Force Ajax header
            axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

            // Setup for axios
            const name = $('#name').val();
            const rating = $('#rating').val();
            const review = $('#review').val();
            e.preventDefault();
            let data = new FormData();
            data.append('name', name);
            data.append('rating', rating);
            data.append('review', review);

            // Data to PHP and display alert
            axios.post('checkinscript.php', data)
                .then(function (response) {
                    console.log(response);
                    $("#success-alert").fadeTo(2000, 500).slideUp(500, function () {
                        $("#success-alert").slideUp(500);
                    })
                    SetRatingStar();
                    // Get the updated HTML from PHP
                    axios.get('checkinscript.php')
                        .then(function (response) {
                            document.getElementById('checkins').innerHTML = response.data;
                        })
                        .catch(function (error) {
                            console.log(error);
                        })
                })
                .catch(function (error) {
                    console.log(error);
                })


    }
    )
}
)