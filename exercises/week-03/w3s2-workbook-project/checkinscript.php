<?php

require_once('checkin.php');

if (!empty($_POST)) {

    if (filter_var($_POST['rating'], FILTER_VALIDATE_INT, array("options" => array("min_range"=>1, "max_range"=>5))) === false) {
        echo 'invalid rating';
        die();
    }

    if (filter_var(strlen($_POST['name']), FILTER_VALIDATE_INT, array("options" => array("min_range"=>3, "max_range"=>20))) === false) {
        echo 'invalid name';
        die();
    }

    if (filter_var(strlen($_POST['review']), FILTER_VALIDATE_INT, array("options" => array("min_range"=>20, "max_range"=>200))) === false) {
        echo 'invalid review';
        die();
    }

    // Build new CheckIn object
    $checkinData = new CheckIn();
    $checkinData->name = strip_tags($_POST['name']);
    $checkinData->rating = strip_tags($_POST['rating']);
    $checkinData->review = strip_tags($_POST['review']);
    $checkinData->timestamp = time();

    // Save submission to file and update reviews section
    $checkinFile = 'check-ins.txt';

    if (file_exists($checkinFile) == false) {
        $checkInsArray = [];
        array_push($checkInsArray, $checkinData);
        $txtData = serialize($checkInsArray);
        file_put_contents($checkinFile, $txtData);

    } else {
        $fileContent = file_get_contents($checkinFile);
        $fileContent = unserialize($fileContent);
        array_push($fileContent, $checkinData);
        $txtData = serialize($fileContent);
        file_put_contents($checkinFile, $txtData);
        foreach ($fileContent as $checkin){
            $date = date('d/m/Y', $checkin->timestamp);
            echo "<h3>$checkin->name  $checkin->rating</h3>";
            echo "<p>$checkin->review</p>";
            echo "<p><small>$date</small></p>";
        }

        }
    die();

}

// Functions
// Convert rating number to stars
function star($rate) {
    $starRating = [];
    $increment = 0;
    $max = 5;

    while($increment < $rate) {
        array_push($starRating, "<i class='fas fa-star fa text-warning pt-1'></i>");
        $increment++;
    }

    while($max > $rate) {
        array_push($starRating, "<i class='fas fa-star fa text-secondary pt-1'></i>");
        $max--;
    }

    foreach ($starRating as $rating) {
        return $starRating;
    }
}

// Calculate and update Overall Rating
function updateOverallRating($average){
    if ($average > 0){
        return star($average);

    } else {

        echo '<h2>No Reviews to Display</h2>';
    }
}


// Get data from check-ins file and prepare HTML for JavaScript
if (file_exists('check-ins.txt') == false) {
    echo "<h3 class='p-3 m-3 border'>No Reviews Available</h3>";
} else {
$text = file_get_contents('check-ins.txt');
$example = unserialize($text);
$total = 0;
$reviewsBodyA = [];
$reviewsBodyB = [];
$starsAdded = [];

foreach ($example as $checkin) {
    $date = date('d/m/Y', $checkin->timestamp);
    $total += $checkin->rating;
    array_push($starsAdded, $checkin->rating);
    array_push($reviewsBodyA, "<h3>$checkin->name</h3>");
    array_push($reviewsBodyB, "<p>$checkin->review</p><p><small>$date</small></p>");
    }
$average = round($total / count($example));
}

?>

<h2 class="p-3">Additional Information</h2>
<div class="border p-3 m-3">
    <table class="table">
        <tbody>
        <tr>
            <th scope="row">Average Rating</th>
            <td>
                    <span id="overallRating">
                        <?php
                        if (file_exists('check-ins.txt') == false) {
                        echo "No Reviews Available";
                        } else {
                            $thisArray = updateOverallRating($average);
                            foreach ($thisArray as $theStarts){
                                echo $theStarts;
                        }
                        ?>
                    </span>
            </td>
        </tr>
        <tr>
            <th scope="row">Yum Yum Rating</th>
            <td>78/100</td>
        </tr>
        <tr>
            <th scope="row">Wow Factor</th>
            <td>Jaw Dropping</td>
        </tr>
        </tbody>
    </table>
</div>
<h2 class="p-3">Recent Checkins</h2>

<?php

$reviewCount = 0;
foreach ($reviewsBodyA as $thisReview){
    echo "<div class='p-3 m-3 border '>";
    echo "<div class='row m-auto ml-md-auto'>";
    echo $thisReview;
    echo '&nbsp;','&nbsp;';
    echo  "<span class='pt-1'>";
    $start = star($starsAdded[$reviewCount]);
    foreach ($start as $here){
        echo $here;
    }
    echo "</span>";
    echo "</div>";
    echo "<span class=' m-auto'>";
    echo $reviewsBodyB[$reviewCount];
    $reviewCount++;
    echo "</span>";
    echo "</div>";

}}




