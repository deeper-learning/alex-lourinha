<?php
if (!empty($_POST)) {

// Takes raw data from the request
    //$json = file_get_contents('php://input');

// Converts it into a PHP object
    //$data = json_decode($json);

    $data = new StdClass();
    $data->a = $_POST['a'];
    $data->b = $_POST['b'];
    $data->op = $_POST['op'];

    $txtData = serialize($data);
    file_put_contents(time() . ".txt", $txtData);

    switch ($data->op) {
        case "add":
            echo $data->a + $data->b;
            break;
        case "subtract":
            echo $data->a - $data->b;
            break;
        case "divide":
            echo $data->a / $data->b;
            break;
        case "multiply":
            echo $data->a * $data->b;
            break;
        default:
            echo "This operator is invalid, please choose: add, multiply, divide or subtract.";
    }
    die();

}

?>

<table id="dynamicTable"  class="table table-hover mx-auto text-center">
    <thead class="bg-warning">
    <th>Timestamp</th>
    <th>First Number</th>
    <th>Operator</th>
    <th>Second Number</th>
    </thead>
    <tbody id="tablebody" class="mx-auto">
<?php

$row = 0;
$id = '1';
foreach (glob("*.txt") as $file) {
    $info = file_get_contents($file);
    if ($info != "N;") {
        $filename = explode(".", $file);
        $results = unserialize($info);
        if ($row % 2 == 0){
            echo "<tr onclick='selectRow(this)' class='table-light order-first' id=$id ><td class='timestamp'>$filename[0]</td><td class='firstNumber'>$results->a</td><td class='operation'>$results->op</td><td class='secondNumber'>$results->b</td></tr>";
        } else {
            echo "<tr onclick='selectRow(this)' class='table-warning order-first' id=$id ><td class='timestamp'>$filename[0]</td><td class='firstNumber'>$results->a</td><td class='operation'>$results->op</td><td class='secondNumber'>$results->b</td></tr>";
        }
        $row += 1;
        $id += 1;

    }
}

