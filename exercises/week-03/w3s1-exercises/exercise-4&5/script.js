
$('#myForm').on('submit',
    function(e){

        // Force Ajax header
        axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

        const a = $('#a').val();
        const b = $('#b').val();
        const op = $('#op').val();
        e.preventDefault();
        let data = new FormData();
        data.append('a',a);
        data.append('b', b);
        data.append('op', op);

        axios.post('calc.php', data)
        .then(resp => {
            $('#total').val(resp.data);

        })
        .catch(err=> console.log(err));

        setTimeout(
            function updateTable(){

                axios.get('calc.php')
                    .then(function (response){
                        $('#resultsTable').html(response.data);
                        $(function(){
                            $("tbody").each(function(elem,index){
                                var arr = $.makeArray($("tr",this).detach());
                                arr.reverse();
                                $(this).append(arr);
                            });
                        });
                    })
            }, 500);

})

setTimeout(
    function updateTable(){

    axios.get('calc.php')
        .then(function (response){
            $('#resultsTable').html(response.data);
            $(function(){
                $("tbody").each(function(elem,index){
                    var arr = $.makeArray($("tr",this).detach());
                    arr.reverse();
                    $(this).append(arr);
                });
            });
        })
    }, 500);

function selectRow(elem){
    let firstNumber = $(elem).children().eq(1).text();
    let operator = $(elem).children().eq(2).text();
    let secondNumber = $(elem).children().eq(3).text();
    $('#a').val(firstNumber);
    $('#b').val(secondNumber);
    $('#op').val(operator);
    $('#total').val("");
    $('#btt').removeAttr('disabled');

}

//document.getElementById("a").onchange = tre();
$( document ).ready(tre);
$('input').change(tre);


function tre() {
    console.log($('#a').val());
    let empty = true;
    if ($('#a').val() != "" && $('#b').val() != "" && $('#op').val() != "") {
        empty = false;
    }


    if (empty) {
        $('#btt').attr('disabled', 'disabled');
    } else {
        $('#btt').removeAttr('disabled');
        console.log('removed')
    }


}