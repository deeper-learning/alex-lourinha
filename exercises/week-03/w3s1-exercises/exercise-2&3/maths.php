<?php

if (!empty($_GET)) {
    $total = 0;
    switch ($_GET["operation"]) {
        case "add":
            $total = $_GET['a']+$_GET['b'];
            echo $_GET["a"] . " + " . $_GET["b"] . " = " . $total;
            break;
        case "subtract":
            $total = $_GET['a']-$_GET['b'];
            echo $_GET["a"] . " - " . $_GET["b"] . " = " . $total;
            break;
        case "divide":
            $total = $_GET['a']/$_GET['b'];
            echo $_GET["a"] . " / " . $_GET["b"] . " = " . $total;
            break;
        case "multiply":
            $total = $_GET['a']*$_GET['b'];
            echo $_GET["a"] . " * " . $_GET["b"] . " = " . $total;
            break;
        default:
            echo "This operator is invalid, please choose: add, multiply, divide or subtract.";
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Maths</title>
</head>
<body>
    <form>
        <label for="1">First Number</label>
        <input id="1" name="a" type="text">
        <label for="2">Second Number</label>
        <input id="2" name="b" type="text">
        <label for="op">Operation</label>
        <input id="op" name="operation" type="text"placeholder="e.g. multiply">
        <button type="submit">Result</button>
    </form>
</body>
</html>