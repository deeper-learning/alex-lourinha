//Use the for loop to output even numbers from 2 to 10.

/*let i = 2;

for (; i <= 10; i++) {
    if (i%2 ==0) {
        alert(i);
    }

}*/

//Rewrite the code changing the for loop to while without altering its behavior
// (the output should stay same).

/*
let i = 2
while (i <= 10) {
    if (i%2 == 0){
        alert(i)
    }
    i++;
}*/

/*Write a loop which prompts for a number greater than 100. If the visitor enters another number –
ask them to input again.

The loop must ask for a number until either the visitor enters a number greater than 100 or
cancels the input/enters an empty line.

Here we can assume that the visitor only inputs numbers. There’s no need to implement a special
handling for a non-numeric input in this task.*/

/*
let num =1;
while (num < 100 && num) {
    num = prompt("Enter a number: ", 0);
}*/

/*Write the code which outputs prime numbers in the interval from 2 to n.

For n = 10 the result will be 2,3,5,7.

P.S. The code should work for any n, not be hard-tuned for any fixed value.*/

let n = prompt("choose a number");

primeNums: for (let i =2 ; i <= n; i++ ) {
    for (let x=2 ; x < i; x++){
        if (i%x == 0) {
            continue primeNums
        }
    }
    alert(i)
}